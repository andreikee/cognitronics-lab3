# -*- coding: utf-8 -*-
"""
Created on Tue May  7 01:18:48 2019

@author: andrei.krivosei
"""


import numpy as np

def ensemble_avg(signals: dict) -> np.ndarray:
    avg = np.mean(signals['data'], axis = 0)
    signals['ens_avg'] = avg                        # Now 'ens_avg' array available in the dictionary 'signals'
        
    return avg                                      # but returns only avg array


def mask_by_threshold(signal: np.ndarray, thres: int = 5) -> dict:
    
    #  for example absolute amplitudes can be filtered by threshold
    mask = np.array([ 0 if abs(x) < thres else 1 for x in signal ])
            
    return {'mask': mask}


def all_masks_by_threshold(signals: dict, thres: int = 5) -> dict:
    
    size = len(signals['sig1'])
    
    # Create empty Numpy 2D array for masks
    masks = np.empty((0,size), int)
    
    masks_dict = {}
    
    key_range = [ 'sig' + str(x) for x in range(1, 8 + 1)]
    
    for key in signals.keys():
        if key in key_range:
            #  for example absolute amplitudes can be filtered by threshold
            sig_thres = mask_by_threshold(signals[key], thres)          # get the mask for the current key
            masks = np.append(masks, [sig_thres['mask']], axis = 0)     # append to the 2D array of masks
            masks_dict['mask'+key[-1]] = sig_thres['mask']              # put into dictionary each mask
            
    masks_dict['masks'] = masks    # all masks as 2D numpy array
    masks_dict['t'] = np.copy(signals['t'])
            
    return masks_dict        # returns dictionary of thresholded masks

