# -*- coding: utf-8 -*-
"""
Created on Wed 01. May 2019
@author: Andrei Krivoshei

"""
import numpy as np

def get_signals(filename):
    """Return a dictionary of signals that has been parsed form file
       filename.
    """
   
    myosig = []     # Init the list for lines from the file
    
    # Read data from the file
    with open(filename, encoding = "utf-8") as file:    # opening the file for reading
        for line in file:                               # iterate throw the lines in file
            if len(line.strip()) > 0:
                myosig.append(line.strip())             # append each non-empty line from the file into the list "myosig"
    
    
    for i in range(0, 20):                              # just prints out the first 20 lines of the file
        print ('line nr: ',i,' - ',myosig[i]),          # pritty print with line numbers
    
    myosig = myosig[2:]                                 # Slice the list (remove first 2 lines)
    
    print("\n", myosig[:10])                            # Print the first 10 items of the list "myosig" as it is
    
    t = np.linspace( 0, len(myosig)/50.0, len(myosig))  # Create time samples vector as numpy array (assuming 50 samples per second)
    
    
    print("\n", t[:10])                                 # Print the first 10 elements of the time vector "t"
    
    
    # Create empty Numpy 2D array for signals
    data = np.empty((0,8), int)
    
    # Iterate through all lines in list of data
    for line in myosig:                                 # one line at a time
        line = line.strip()[1:-1]                       # remove the first and the last brackets in the line
        
        # transform the list of strings into numpy array of int numbers
        # using two brackets "][" as array elements separator
        line = np.fromstring( line, dtype = np.int, sep='][' )
        
        # append the line as 1D np.array to 2D np.array "data"
        # "[line]" - do the 2D array from 1D array with 1 row
        data = np.append(data, [line], axis = 0)
        
    # transpose the data, Now each row of the data is the signal
    data = data.transpose()
    
    # Init the dictionary
    result={}
    result['t'] = t             # put the time array as the related dictionary record
    
    # Iterate through all signals in the data
    for i in range(len(data)):
        result['sig' + str(i+1)] = data[i]      # put each signal array as related dictionary record
    
    result['data'] = data
    
    return result

