# -*- coding: utf-8 -*-
"""
Created on Tue May  7 00:31:49 2019

@author: Andrei Krivosei
"""

import myo_signal
import plot_signals as plt_sig

import numpy as np
from matplotlib import pyplot as plt

# !!!!! Name your measured gestures files like 'G1..G7'. 'G' and index of gesture

file = 'G5'  # file name without extension
path = 'Data\\' # if you don't need this put empty string insteed
ext = '.txt'
filename = path + file + ext

# !!! Gesture ID. Must be new value for new gesture
g = int(file[-1]) # use the last symbol from the file name 'Gx', x is a number

signals=myo_signal.get_signals(filename)

# Plot the all the signals or some selected signal
plt_sig.plot_signals(signals, 1, 8, title = 'Signals from ' + filename)

