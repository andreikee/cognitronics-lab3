# -*- coding: utf-8 -*-
"""
Created on Tue May  7 00:35:26 2019

@author: andrei.krivosei
"""

from matplotlib import pyplot as plt

def plot_signals(signals: dict, title: str = "Signals", plots_names: list = ['']) -> None:      # signals is of type dictionary
    
    # Plot the all the signals or some selected signal
    for key in signals.keys():
        if key in plots_names:   # selects, which signals to plot
            plt.plot(signals['t'], signals[key], label = key)
            
    # Some parameters for the plot
    plt.title(title)
    plt.xlabel('Time, s')
    plt.legend()
    plt.show()