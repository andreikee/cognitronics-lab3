# -*- coding: utf-8 -*-
"""
Created on Wed 01. May 2019
@author: Andrei Krivoshei
"""
import os
from matplotlib import pyplot as plt

import myo_signal
import signal_filters as sf
import plot_signals as plt_sig


# !!! Put your file path and name
# filename='###put Your filename and path here###'

filename = os.path.join('Data', 'G1.txt')

signals = myo_signal.get_signals(filename)

sf.ensemble_avg(signals)
masks = sf.all_masks_by_threshold(signals)

plots_range = [ 'sig' + str(x) for x in range(1, 3 + 1)]

# Plot the all the signals or some selected signal
plt_sig.plot_signals(signals, 'Signals from ' + filename, plots_range + ['ens_avg'])

masks_range = [ 'mask' + str(x) for x in range(1, 8 + 1)]

plt_sig.plot_signals(masks, 'Masks', masks_range)



