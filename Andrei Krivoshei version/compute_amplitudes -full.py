# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 14:33:47 2017

@author: juhan
Modified by Andrei Krivoshei 01.05.2019
"""

import myo_signal
import numpy as np
from matplotlib import pyplot as plt
import csv


# !!!!! Name your measured gestures files like 'G1..G7'. 'G' and index of gesture

file = 'G5'  # file name without extension
path = 'Data\\' # if you don't need this put empty string insteed
ext = '.txt'
filename = path + file + ext

# !!! Gesture ID. Must be new value for new gesture
g = int(file[-1]) # use the last symbol from the file name 'Gx', x is a number

signals=myo_signal.get_signals(filename)

# Plot the all the signals or some selected signal
for key, values in signals.items():
    if key != 't':
#    if key != 't' and key == 'sig3':   # use this line if you want plot only one signal
        plt.plot(signals['t'], values, label = key)
        
# Some parameters for the plot
plt.title('Signals from ' + filename)
plt.xlabel('Time, s')
plt.legend()
plt.show()

# Plot a single signal:
#plt.plot(signals['t'], signals['sig1'])


# Function for signals filtering. Choose some algorithm for filtering
def filter_signal(signal):
    result = np.empty(len(signal), float)
    
    # Compute the absolute values of a signal
    # using list comprehension
    
    sig_abs=[abs(x) for x in signal]    # as option
    
    # You can compute the average of the absolute values
    sig_avg = sum(sig_abs)/len(sig_abs)  # as option
    
    # It is possible to set values below thres to be 0
    # using the following list comprehension:
    
    thres = 5
    #  for example absolute amplitudes can be filtered by threshold
    sig_thres = [ 0 if abs(x) < thres else x for x in signal ]
    
    result = sig_thres   # use thresholde signal as output
    
    return result


filtered_sig1 = filter_signal(signals['sig1'])
filtered_sig2 = filter_signal(signals['sig2'])
filtered_sig3 = filter_signal(signals['sig3'])
filtered_sig4 = filter_signal(signals['sig4'])
filtered_sig5 = filter_signal(signals['sig5'])
filtered_sig6 = filter_signal(signals['sig6'])
filtered_sig7 = filter_signal(signals['sig7'])
filtered_sig8 = filter_signal(signals['sig8'])
# ... other signals


# Plot the filtered
plt.plot(signals['t'], filtered_sig1)


# Make all the channels have nonzero
# signals at the same time
for i in range(len(filtered_sig1)):
    if filtered_sig1[i] > 0:
        filtered_sig2[i] = signals['sig2'][i]
        filtered_sig3[i] = signals['sig3'][i]
        filtered_sig4[i] = signals['sig4'][i]
        filtered_sig5[i] = signals['sig5'][i]
        filtered_sig6[i] = signals['sig6'][i]
        filtered_sig7[i] = signals['sig7'][i]
        filtered_sig8[i] = signals['sig8'][i]
        # ... other signals

def compute_amplitude(sig,current_index,window_length=20):
    """ Computes the amplitude of the signal starting at current_index
        and iterating window_length steps ahead in the signal. NB! window_lenght
        should not be set to zero (default is 20).
    """
    sum_sig = 0
    for j in range(window_length): # Take 20 samples, i.e. 1 second interval
         sum_sig = sum_sig + abs( sig[current_index+j] )
    amplitude = sum_sig / window_length
    return amplitude

        
# Compute amplitudes of gesture 1 taking 1 second interval after 5 instances have been nonzero:
i = 0
amplitudes = []

# Adjust the skip and window parameters according to your data.
skip = 30
window = 50

while i < len(filtered_sig1) - window - skip:
    res=[0]*9 # Initialize result vector res to be a list of 9 zeroes.
              # res[0] is for gesture.
              # res[1] is for amplitude of sig1 in the particular gesture,
              # res[2] is for amplitude of sig2 in the particular gesture, etc
    if ( filtered_sig1[i] != 0 and
         filtered_sig1[i+1] != 0 and
         filtered_sig1[i+2] != 0 and
         filtered_sig1[i+3] != 0):
        
        i = i + skip
        res[0] = g # current gesture ID
        res[1] = compute_amplitude(filtered_sig1, i, window)
        res[2] = compute_amplitude(filtered_sig2, i, window)
        res[3] = compute_amplitude(filtered_sig3, i, window)
        res[4] = compute_amplitude(filtered_sig4, i, window)
        res[5] = compute_amplitude(filtered_sig5, i, window)
        res[6] = compute_amplitude(filtered_sig6, i, window)
        res[7] = compute_amplitude(filtered_sig7, i, window)
        res[8] = compute_amplitude(filtered_sig8, i, window)
        # ... other signals

        amplitudes.append(res) # Store the current result in the amplitudes list of lists
        i = i + window
        # Now we skip the signal until there is at least 3 consecutive zeros.
        while ( i < (len(filtered_sig1)-4) and 
                not (filtered_sig1[i] == 0 and
                     filtered_sig1[i+1] == 0 and
                     filtered_sig1[i+2] == 0 and
                     filtered_sig1[i+3] == 0 and
                     filtered_sig1[i+4] == 0)):
            i = i + 1
    else:
        i = i + 1



# Writing and reading CSV files:
# https://docs.python.org/3/library/csv.html

with open('gesture_' + str(g) + '_amplitudes.csv','w', newline='') as csvfile:
    gwriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    # Write the header
    gwriter.writerow(['gesture','A(sig1)','A(sig2)','A(sig3)','A(sig4)','A(sig5)','A(sig6)','A(sig7)','A(sig8)'])
    # Write all the rows as stored in the amplitudes list of lists at once
    gwriter.writerows(amplitudes)














