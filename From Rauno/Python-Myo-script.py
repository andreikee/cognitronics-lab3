# -*- coding: utf-8 -*-
"""
Created on Wed 26. October 2016
@author: Rauno
"""
import numpy as np
from matplotlib import pyplot as plt

filename='###put Your filename and path here###'
fid = open(filename, 'r')   #opening the file for reading
myosig = fid.readlines()    #reads lines as strings, C becomes a string array
fid.close()

for i in range(0,len(myosig)):    #just prints out the lines of the file
    if len(myosig[i]) > 0:
        print ('line nr: ',i,' - ',myosig[i]),

#a = [];     s = [];
sig1=[];    sig2=[];    sig3=[];    sig4=[];
sig5=[];    sig6=[];    sig7=[];    sig8=[];
t = np.linspace(0, (len(myosig)-4)/float(50), len(myosig)-4)     # assuming 50 samples per second
for j in range (4,len(myosig)):    #one line at a time, finding numbers
    ab = myosig[j]
    sig1.append(ab[1:4].split())
    sig2.append(ab[7:10].split())
    sig3.append(ab[13:16].split())
    sig4.append(ab[19:22].split())
    sig5.append(ab[25:28].split())
    sig6.append(ab[31:34].split())
    sig7.append(ab[37:40].split())
    sig8.append(ab[43:46].split())
#sig1 = np.transpose(sig1)
plt.plot(t,sig1,t,sig2,t,sig3,t,sig4,t,sig5,t,sig6,t,sig7,t,sig8)
