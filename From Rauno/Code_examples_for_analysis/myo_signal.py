# -*- coding: utf-8 -*-
"""
Created on Wed 26. October 2016
@author: Rauno

Modified by Juhan on April 27, 2017

"""
import numpy as np
from matplotlib import pyplot as plt

def flatten(l):
    """The problem with sig1, sig2 etc is that it is currently
       list of lists of strings, e.g. [['-1'],['0']]
       To get rid of that, you can use the flatten function"""
    return [int(val) for sublist in l for val in sublist]


def get_signals(filename):
    """Return a dictionary of signals that has been parsed form file
       filename.
    """
    fid = open(filename, 'r')   #opening the file for reading
    myosig = fid.readlines()    #reads lines as trings, C becomes a string array
    fid.close()

    for i in range(0,len(myosig)):    #just prints out the lines of the file
        if len(myosig[i]) > 0:
             print('line nr: ',i,' - ',myosig[i])

#a = [];     s = [];
    sig1=[];    sig2=[];    sig3=[];    sig4=[];
    sig5=[];    sig6=[];    sig7=[];    sig8=[];
    t = np.linspace(0, (len(myosig)-4)/float(50), len(myosig)-4)     # assuming 50 samples per second
    for j in range (4,len(myosig)):    #one line at a time, finding numbers
        ab = myosig[j]
        sig1.append(ab[1:4].split())
        sig2.append(ab[7:10].split())
        sig3.append(ab[13:16].split())
        sig4.append(ab[19:22].split())
        sig5.append(ab[25:28].split())
        sig6.append(ab[31:34].split())
        sig7.append(ab[37:40].split())
        sig8.append(ab[43:46].split())
    result={}
    result['sig1']=flatten(sig1)
    result['sig2']=flatten(sig2)
    result['sig3']=flatten(sig3)
    result['sig4']=flatten(sig4)
    result['sig5']=flatten(sig5)
    result['sig6']=flatten(sig6)
    result['sig7']=flatten(sig7)
    result['sig8']=flatten(sig8)
    result['t']=t
    return result

