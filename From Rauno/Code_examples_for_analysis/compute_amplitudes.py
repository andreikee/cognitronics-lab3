# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 14:33:47 2017

@author: juhan
"""

import myo_signal

signals=myo_signal.get_signals('myo-andmed1.txt')

import numpy as np
from matplotlib import pyplot as plt

t=signals['t']
plt.plot(t,signals['sig1'],t,signals['sig2'],t,signals['sig3'],t,signals['sig4'])
plt.plot(t,signals['sig5'],t,signals['sig6'],t,signals['sig7'],t,signals['sig8'])


# Plot a single signal:
plt.plot(t,signals['sig1'])

# Compute the absolute values of a signal
# using list comprehension
abssig1=[abs(x) for x in signals['sig1']]

# You can compute the average of the absolute values
sum(abssig1)/len(abssig1)

# It is possible to set values below thres to be 0
# using the following list comprehension:
thres=5
filtered_sig1=[ 0 if abs(x)<thres else x for x in signals['sig1']]

# Plot the filtered
plt.plot(t,filtered_sig1)

# Initialize filtered signal vectors to containing zeroes.
filtered_sig2=[0]*len(filtered_sig1)
filtered_sig3=[0]*len(filtered_sig1)
filtered_sig4=[0]*len(filtered_sig1)
filtered_sig5=[0]*len(filtered_sig1)
filtered_sig6=[0]*len(filtered_sig1)
filtered_sig7=[0]*len(filtered_sig1)
filtered_sig8=[0]*len(filtered_sig1)

# Make all the channels have nonzero
# signals at the same time
for i in range(len(filtered_sig1)):
    if filtered_sig1[i]>0:
        filtered_sig2[i]=signals['sig2'][i]
        filtered_sig3[i]=signals['sig3'][i]
        filtered_sig4[i]=signals['sig4'][i]
        filtered_sig5[i]=signals['sig5'][i]
        filtered_sig6[i]=signals['sig6'][i]
        filtered_sig7[i]=signals['sig7'][i]
        filtered_sig8[i]=signals['sig8'][i]

def compute_amplitude(sig,current_index,window_length=20):
    """ Computes the amplitude of the signal starting at current_index
        and iterating window_length steps ahead in the signal. NB! window_lenght
        should not be set to zero (default is 20).
    """
    sum_sig=0
    for j in range(window_length): # Take 20 samples, i.e. 1 second interval
         sum_sig=sum_sig+abs(sig[current_index+j])
    amplitude = sum_sig/window_length
    return amplitude

        
# Compute amplitudes of gesture 1 taking 1 second interval after 5 instances have been nonzero:
i=0
g=0 # I assume we are currently talking about gesture 0
amplitudes=[]

# Adjust the skip and window parameters according to your data.
skip=30
window=50

while i<len(filtered_sig1)-window-skip:
    res=[0]*9 # Initialize result vector res to be a list of 9 zeroes.
              # res[0] is for gesture.
              # res[1] is for amplitude of sig1 in the particular gesture,
              # res[2] is for amplitude of sig2 in the particular gesture, etc
    if ( filtered_sig1[i]!=0 and
         filtered_sig1[i+1]!=0 and
         filtered_sig1[i+2]!=0 and
         filtered_sig1[i+3]!=0):
        i=i+skip
        res[0]=g
        res[1]=compute_amplitude(filtered_sig1,i,window)
        res[2]=compute_amplitude(filtered_sig2,i,window)
        res[3]=compute_amplitude(filtered_sig3,i,window)
        res[4]=compute_amplitude(filtered_sig4,i,window)
        res[5]=compute_amplitude(filtered_sig5,i,window)
        res[6]=compute_amplitude(filtered_sig6,i,window)
        res[7]=compute_amplitude(filtered_sig7,i,window)
        res[8]=compute_amplitude(filtered_sig8,i,window)
        amplitudes.append(res) # Store the current result in the amplitudes list of lists
        i=i+window
        # Now we skip the signal until there is at least 3 consecutive zeros.
        while ( i<(len(filtered_sig1)-4) and 
                not (filtered_sig1[i]==0 and
                     filtered_sig1[i+1]==0 and
                     filtered_sig1[i+2]==0 and
                     filtered_sig1[i+3]==0 and
                     filtered_sig1[i+4]==0)):
            i=i+1
    else:
        i=i+1



# Writing and reading CSV files:
# https://docs.python.org/3/library/csv.html
import csv
with open('gestureamplitudes.csv','w', newline='') as csvfile:
    gwriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    # Write the header
    gwriter.writerow(['gesture','A(sig1)','A(sig2)','A(sig3)','A(sig4)','A(sig5)','A(sig6)','A(sig7)','A(sig8)'])
    # Write all the rows as stored in the amplitudes list of lists at once
    gwriter.writerows(amplitudes)














