# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 15:20:23 2017

@author: juhan
"""

# Read data from CSV file:

import csv
with open('gestureamplitudes.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        print(row)

# Our data has headers, so we can skip that.
# Now let's read the data into a gesture vector G 
# and amplitude data D:

## D is the sample data, 8 amplitude values per row.
#D= [[0, 0,0,0,0,0,0,0], [0, 0,1,0,0,0,1,0], [1, 1,1,1,1,1,1,1], [2,2,2,2,2,2,2,2], [3,3,3,3,3,3,3,3]]
## G represents the gestures encoded by integers
#G= [0,0,1,2,3]

D=[]
G=[]

with open('gestureamplitudes.csv', newline='') as f:
    reader = csv.reader(f)
    header = True
    for row in reader:
        if header:
            header=False
            continue
        G.append(row[0])
        D.append(row[1:])
        

# Let us shuffle the data to make sure the order of rows is random, but G and D are still
# correlated:

from sklearn.utils import shuffle
G,D= shuffle(G,D)



# For decision tree classification use the following code:
from sklearn import tree
# The D[:3] syntax means that we take items from D with indexes
# less than the number (3 in this case):
X = D[:3]
Y = G[:3]

# Instantiation of the classifier
dt = tree.DecisionTreeClassifier()
# Training of the classifier object instance:
dt = dt.fit(X, Y)

#After being fitted, the model can then be used to predict the class of samples:
dt.predict([[0., 0., 1., 0.,0.,0.,0.,0.]])

# Score provides the accuracy result of the classifier.
# It should be fed test data that the classifier 
# has not seen during training
# NB! You should pass more than one sample to score!
dt.score([[0., 0., 1., 0.,0.,0.,0.,0.]],[0])

from sklearn.neighbors import KNeighborsClassifier

knn = KNeighborsClassifier()

knn.fit(X,Y)

knn.predict([[0., 0., 1., 0.,0.,0.,0.,0.]])

knn.score([[0., 0., 1., 0.,0.,0.,0.,0.]],[0])


from sklearn import ensemble

rf = ensemble.RandomForestClassifier()
rf.fit(X,Y)
rf.predict([[0., 0., 1., 0.,0.,0.,0.,0.]])
rf.score([[0., 0., 1., 0.,0.,0.,0.,0.]],[0])



# Regression model. Needs additional work for classification.

from sklearn import linear_model

linear = linear_model.LinearRegression()
linear.fit(X,Y)

linear.predict([[0., 0., 1., 0.,0.,0.,0.,0.]])

#linear.score([[0., 0., 1., 0.,0.,0.,0.,0.]],[0])